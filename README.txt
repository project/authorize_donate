
About

This module implements the donation using the server integration method (SIM) for card not present transactions, by Authorize.net
 
Prerequisites
																	
MHASH
The required MHASH extension comes with PHP and
requires no additional cost. 
 
Most web hosting providers install this extension
along with PHP; however, in some circumstances, you
may have to enable it yourself or ask your web host
to enable it for you.
 
